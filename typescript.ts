const calculator = document.getElementById('calculator');
let display: HTMLInputElement = document.getElementById("display") as HTMLInputElement;
let memory: number = 0;

const getValue = (userInput: String): string => {
    return display.value != null ? display.value += userInput : ' ';
}

const getResult = (): string => {
    return display.value;
}

const result = (): string => {
    try {
        let x = getResult();
        return display.value = eval(x);
    } catch {
        return display.value = "Syntax Error!"
    }
}

const F_E = (): string => {
    let value = getResult();
    let x = parseFloat(value).toExponential(10);
    return display.value = x.toString();
}

const degtorad = (): string => {
    let value = getResult();
    return display.value = (Number(value) * (Math.PI / 180)).toString()
}

const pi = (): string => {
    let value: string = getResult();
    let x: number = Number(value) * Math.PI;
    return display.value = x.toString();
}

const e = (): string => {
    let value: string = getResult();
    let x: number = Number(value) * Math.E;
    return display.value = x.toString();
}


const square = (): string => {
    let value: string = getResult();
    let x: number = Math.pow(Number(value), 2);
    return display.value = x.toString()
}


const reciprocal = (): string => {
    let value: string = getResult();
    let x: number = Math.pow(Number(value), -1);
    return display.value = x.toString()
}

const mod = (): string => {
    let value: string = getResult();
    let x: number = Math.abs(Number(value));
    return display.value = x.toString()
}

const exp = (): string => {
    let value: string = getResult();
    let x: number = Math.exp(Number(value));
    return display.value = x.toString()
}

const sqrt = (): string => {
    let value: string = getResult();
    let x: number = Math.sqrt(Number(value));
    return display.value = x.toString()
}

const factorial = () => {
    let value: string = getResult();
    let fact: number = 1;
    for (let i: number = 1; i <= Number(value); i++) {
        fact = fact * i;
        display.value = fact.toString();
    }
}

const xpowOf10 = (): string => {
    let value: string = getResult();
    let x: number = Math.pow(10, Number(value));
    return display.value = x.toString()
}

const log = (): string => {
    let value: string = getResult();
    let x: number = Math.log(Number(value));
    return display.value = x.toString();
}

const ln = (): string => {
    let value: string = getResult();
    let x: number = Math.LN10 * Number(value);
    return display.value = x.toString();
}

const deleteValue = (): string => {
    let length: number = display.value.length;
    return display.value = display.value.substring(0, length - 1)
}
const sin = (): string => {
    let value: string = getResult();
    let x: number = Math.sin(Number(value));
    return display.value = x.toString();
}
const cos = (): string => {
    let value: string = getResult();
    let x: number = Math.cos(Number(value));
    return display.value = x.toString();
}
const tan = (): string => {
    let value: string = getResult();
    let x: number = Math.tan(Number(value));
    return display.value = x.toString();
}


const abs = (): string => {
    let value: string = getResult();
    let x: number = Math.abs(Number(value));
    return display.value = x.toString();
}

const asinh = (): string => {
    let value: string = getResult();
    let x: number = Math.asinh(Number(value));
    return display.value = x.toString();
}

const acosh = (): string => {
    let value: string = getResult();
    let x: number = Math.acosh(Number(value));
    return display.value = x.toString();
}

const atanh = (): string => {
    let value: string = getResult();
    let x: number = Math.asinh(Number(value));
    return display.value = x.toString();
}


const MemoryStore = (): number => {
    let value: string = getResult();
    return memory = Number(value);
}

const MemoryClear = (): void => {
    display.value = ""
}

const MemoryRecall = (): string => {
    let value: string = getResult();
    return display.value = memory.toString();
}

const MemoryPlus = (): number => {
    let value: string = getResult();
    return memory = eval(`${memory} + ${value}`)
}
const MemoryMinus = (): number => {
    let value: string = getResult();
    return memory = eval(`${memory} - ${value}`)
}